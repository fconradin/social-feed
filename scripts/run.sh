# all migrationsfiles have already been created, so no need to create them again
python manage.py migrate
python manage.py collectstatic --noinput
exec /opt/conda/envs/app/bin/uwsgi --ini /scripts/uwsgi.ini #this replaces the runserver command