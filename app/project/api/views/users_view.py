from rest_framework.response import Response
from project.api.serializers.user_serializers import UserSerializer
from project.api.views.master_view import MasterView
from project.feed.models import User


class UsersView(MasterView):

    def get(self, request):
        search_string = request.GET.get('search')
        if search_string:
            users = User.objects.filter(username__icontains=search_string)
        else:
            users = User.objects.all()
        serializer = UserSerializer(users, many=True)
        return Response(serializer.data)


class SpecificUserView(MasterView):

    def get(self, request, user_id):
        user = self.get_object(User, user_id)
        serializer = UserSerializer(user)
        return Response(serializer.data)


class FollowersView(MasterView):

    def get(self, request):
        # user_profile__followees__id=request.user.id <-- alternative filter with same result
        all_followers = User.objects.filter(id__in=request.user.followers.values('user'))
        serializer = UserSerializer(all_followers, many=True)
        return Response(serializer.data)


class FollowingView(MasterView):

    def get(self, request):
        user_profile = self.get_user_profile(request.user)
        followees = user_profile.followees.all()
        serializer = UserSerializer(followees, many=True)
        return Response(serializer.data)


class Follow(MasterView):

    def post(self, request, user_id):
        user_profile = self.get_user_profile(request.user)
        followee = self.get_object(User, user_id)
        user_profile.followees.add(followee)
        return Response(f'You are now following user {user_id}')

    def delete(self, request, user_id):
        user_profile = self.get_user_profile(request.user)
        followee = self.get_object(User, user_id)
        if followee in user_profile.followees.all():
            user_profile.followees.remove(followee)
            return Response(f'You are no longer following user {user_id}')
        else:
            return Response(f'You were never following user {user_id}')
