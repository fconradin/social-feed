from django.contrib.auth import get_user_model
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from project.api.serializers.registration_serializers import PasswordResetValidationSerializer,\
    RegistrationSerializer, PasswordResetSerializer, RegistrationValidation


User = get_user_model()


class RegistrationView(GenericAPIView):
    permission_classes = []
    serializer_class = RegistrationSerializer

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        new_user = serializer.register_user(
            email=serializer.validated_data.get('email'),
        )
        return Response(self.get_serializer(new_user).data)


class RegistrationValidationView(GenericAPIView):
    permission_classes = []
    serializer_class = RegistrationValidation

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        updated_user = serializer.save(serializer.validated_data)
        return Response(self.get_serializer(updated_user).data)


class ResetPasswordView(GenericAPIView):
    authentication_classes = []
    permission_classes = []
    serializer_class = PasswordResetSerializer

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        # there is a user object in here, not an email --> how to make
        serializer.change_token(serializer.validated_data.get('email'))
        return Response('Email has been sent')


class ResetPasswordValidate(GenericAPIView):
    authentication_classes = []
    permission_classes = []
    serializer_class = PasswordResetValidationSerializer

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.update_password(serializer.validated_data)
        return Response('Password has been updated')
