from django.contrib.auth import get_user_model
from django.db.models import Q
from django.http import Http404

from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from project.api.serializers.feed_serializers import FeedDisplaySerializer
from project.api.serializers.post_serializers import PostSerializer
from project.feed.models import Post, UserProfile, FriendRequest

User = get_user_model()


class FeedDisplayView(APIView):
    permission_classes = [
        IsAuthenticated,
    ]

    def get(self, request, **kwargs):
        search_string = request.GET.get('search')
        if not search_string:
            posts = Post.objects.all()
        else:
            posts = Post.objects.filter(content__icontains=search_string)
        serializer = FeedDisplaySerializer(posts, many=True)  # many=True means we are passing a list of obj
        return Response(serializer.data)


class FeedDisplayViewUser(FeedDisplayView):
    permission_classes = [
        IsAuthenticated,
    ]

    def get(self, request, **kwargs):
        user_id = kwargs.get('user_id')
        try:
            user = User.objects.get(id=user_id)
        except User.DoesNotExist:
            raise Http404

        posts = Post.objects.filter(user=user)
        serializer = FeedDisplaySerializer(posts, many=True)  # many=True means we are passing a list of obj
        return Response(serializer.data)


class FeedDisplayViewFollowee(APIView):
    permission_classes = [
        IsAuthenticated,
    ]

    def get(self, request):
        UP = UserProfile.objects.get(user=request.user)
        followees = UP.followees.all()
        posts = Post.objects.filter(user__in=followees)
        serializer = FeedDisplaySerializer(posts, many=True)
        return Response(serializer.data)


class FeedDisplayFriends(APIView):
    permission_classes = [
        IsAuthenticated,
    ]

    def get(self, request):
        receivers = FriendRequest.objects.filter(sender=request.user, status="ACCEPTED").values('receiver')
        senders = FriendRequest.objects.filter(receiver=request.user, status="ACCEPTED").values('sender')
        posts = Post.objects.filter(Q(user__in=senders) | Q(user__in=receivers))
        serializer = PostSerializer(posts, many=True)
        return Response(serializer.data)
