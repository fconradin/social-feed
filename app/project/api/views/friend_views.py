from django.contrib.auth import get_user_model
from django.db import IntegrityError
from django.db.models import Q
from django.http import Http404
from rest_framework.exceptions import MethodNotAllowed
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from project.api.serializers.friend_serializers import FriendRequestSerializer
from project.api.serializers.user_serializers import UserSerializer
from project.api.views.users_view import Follow
from project.feed.models import FriendRequest

User = get_user_model()


class SendRequest(Follow):
    permission_classes = [IsAuthenticated]

    def post(self, request, user_id):
        receiver = self.get_user(user_id)
        try:
            FriendRequest.objects.create(sender=request.user,
                                         receiver=receiver,
                                         status='PENDING')
            return Response('Friend request sent')
        except IntegrityError:
            return Response("Friend request has already been sent to this user")

    def get(self, request):
        raise MethodNotAllowed


class Request(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        pending_requests = FriendRequest.objects.filter(receiver=request.user, status='PENDING').all()
        serializer = FriendRequestSerializer(pending_requests, many=True)
        return Response(serializer.data)


# Todo make super master class with this method inside
class FriendRequestMaster(APIView):
    permission_classes = [IsAuthenticated]

    def get_object(self, model, pk):
        try:
            return model.objects.get(pk=pk)
        except model.DoesNotExist:
            raise Http404(f"{model.__name__} does not have {pk}")


class AcceptRequest(FriendRequestMaster):

    def post(self, request, request_id):
        friend_request = self.get_object(FriendRequest, request_id)
        if friend_request.receiver is not request.user:
            return Response("Friend request not for you")
        if friend_request.status is not "PENDING":
            return Response("Friend request not pending")
        friend_request.status = "ACCEPTED"
        friend_request.save()
        return Response("Friend request accepted")


class RejectRequest(FriendRequestMaster):
    permission_classes = [IsAuthenticated]

    def post(self, request, request_id):
        friend_request = self.get_object(FriendRequest, pk=request_id)
        if friend_request.receiver is not request.user:
            return Response("Friend request not for you")
        if friend_request.status == "PENDING":
            friend_request.status = "REFUSED"
            friend_request.save()
            return Response("Friend request refused")
        else:
            return Response("This is not a pending request")


class Friends(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        accepted_receivers = FriendRequest.objects.filter(sender=request.user, status="ACCEPTED").values('receiver')
        accepted_senders = FriendRequest.objects.filter(receiver=request.user, status="ACCEPTED").values('sender')
        friends = User.objects.filter(Q(id__in=accepted_receivers) | Q(id__in=accepted_senders))
        srlzr = UserSerializer(friends, many=True)
        return Response(srlzr.data)


class Unfriend(FriendRequestMaster):
    permission_classes = [IsAuthenticated]

    def post(self, request, user_id):
        person_to_unfriend = self.get_object(User, pk=user_id)
        to_delete = FriendRequest.objects.filter(Q(sender=person_to_unfriend) |
                                                 Q(receiver=person_to_unfriend))
        to_delete.delete()
        return Response("OK")
