from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from project.api.serializers.me_serializers import MeSerializer
from project.api.serializers.user_serializers import PrivateUserSerializer


class MeView(APIView):
    permission_classes = [
        IsAuthenticated,
    ]

    def get(self, request):
        user = request.user
        serializer = PrivateUserSerializer(user)
        return Response(serializer.data)

    def post(self, request):
        data = request.data
        serializer = MeSerializer(request.user, data=data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response('User updated')
