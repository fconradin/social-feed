from django.http import Http404
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView

from project.feed.models import User, UserProfile


class MasterView(APIView):
    permission_classes = [IsAuthenticated]

    def get_object(self, model, pk):
        try:
            obj = model.objects.get(id=pk)
        except User.DoesNotExist:
            raise Http404(f"Model {model.__name__} does not object with id '{pk}'.")
        return obj

    def get_user_profile(self, user):
        try:
            user_profile = UserProfile.objects.get(user=user)
        except UserProfile.DoesNotExist:
            user_profile = UserProfile.objects.create(user=user)
        return user_profile
