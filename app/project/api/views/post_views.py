from django.contrib.auth import get_user_model
from django.db import IntegrityError
from django.db.models import Count
from django.http import Http404
from rest_framework.permissions import IsAuthenticatedOrReadOnly, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from project.api.permissions import IsOwnerOrReadOnly
from project.feed.models import Post, Like
from project.api.serializers.post_serializers import PostSerializer, PostLikesSerializer

User = get_user_model()


class GetUpdateDeleteView(APIView):
    permission_classes = [
        IsAuthenticatedOrReadOnly,
        IsOwnerOrReadOnly,
    ]

    def get_object(self, **kwargs):
        try:
            post = Post.objects.get(**kwargs)
        except Post.DoesNotExist:
            raise Http404
        return post

    def get(self, request, post_id):
        post = self.get_object(id=post_id)
        serializer = PostSerializer(post)
        return Response(serializer.data)

    def post(self, request, post_id):
        post = self.get_object(id=post_id)
        serializer = PostSerializer(post, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)

    def delete(self, request, post_id):
        post = self.get_object(id=post_id)
        post.delete()
        return Response('OK')


class LikeView(GetUpdateDeleteView):

    def post(self, request, post_id):
        post = self.get_object(id=post_id)  # Method defined in parent class
        try:
            Like.objects.create(post=post, user=request.user)
        except IntegrityError:
            return Response(f'{post.content} has already been liked by {request.user.username}')
        return Response("OK")


class UnLike(GetUpdateDeleteView):

    def post(self, request, post_id):
        post = self.get_object(id=post_id)  # Method defined in parent class
        like = Like.objects.get(post=post, user=request.user)  # if the like does not exist, this will raise an error
        like.delete()
        return Response("OK")


class Share(GetUpdateDeleteView):
    def post(self, request, post_id):
        post = self.get_object(id=post_id)
        serializer = PostSerializer(
            data=request.data,
            context={'request': request, 'shared': post},
        )
        serializer.is_valid(raise_exception=True)
        shared_post = serializer.create(serializer.validated_data)
        return Response(PostSerializer(shared_post).data)


class CreateView(APIView):
    permission_classes = [
        IsAuthenticated,
    ]

    def post(self, request):
        serializer = PostSerializer(
            data=request.data,
            context={'request': request, 'shared_post': None}
        )
        serializer.is_valid(raise_exception=True)
        post = serializer.create(serializer.validated_data)
        return Response(PostSerializer(post).data)


class Likes(APIView):
    permission_classes = [
        IsAuthenticated,
    ]

    def get(self, request):
        posts = Post.objects.filter(likes__gt=0).annotate(num_likes=Count('likes'))
        serializer = PostLikesSerializer(posts, many=True)  # many=True means we are passing a list of obj
        return Response(serializer.data)  # I can just return the data and it looks the same ??
