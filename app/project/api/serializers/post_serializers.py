from django.contrib.auth import get_user_model
from rest_framework import serializers

from project.feed.models import Post

User = get_user_model()


class SharedPostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = ['id', 'content', 'created']
        read_only_fields = fields


class PostSerializer(serializers.ModelSerializer):
    shared = SharedPostSerializer(
        read_only=True
    )

    class Meta:
        model = Post
        fields = ['id', 'content', 'created', 'shared']
        read_only_fields = ['id', 'created', 'shared']

    def create(self, validated_data):
        return Post.objects.create(
            **validated_data,
            user=self.context.get('request').user,
            shared=self.context.get('shared'),
        )

    def validate(self, data):
        if 'content' in data and self.context.get('post') is not None:
            raise serializers.ValidationError("Shared post cannot have content")
        return data


class PostLikesSerializer(serializers.ModelSerializer):
    num_likes = serializers.IntegerField(required=False, read_only=True)

    class Meta:
        model = Post
        fields = ['content', 'num_likes']
