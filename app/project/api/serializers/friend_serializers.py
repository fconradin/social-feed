from rest_framework import serializers
from project.api.serializers.user_serializers import UserSerializer
from project.feed.models import FriendRequest


class FriendRequestSerializer(serializers.ModelSerializer):
    sender = UserSerializer()
    receiver = UserSerializer()

    class Meta:
        model = FriendRequest
        fields = ['sender', 'receiver', 'status']
        read_only_fields = fields
