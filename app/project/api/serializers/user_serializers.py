from django.contrib.auth import get_user_model
from rest_framework import serializers
from project.feed.models import UserProfile

User = get_user_model()


class UserSerializer(serializers.ModelSerializer):
    post_count = serializers.IntegerField(
        label='post count',
        read_only=True
    )

    fame_index = serializers.IntegerField(
        label='fame index',
        read_only=True
    )

    class Meta:
        model = User
        fields = ['id', 'username', 'post_count', 'fame_index']
        read_only_fields = fields

    def to_representation(self, user):
        data = super().to_representation(user)
        all_user_posts = user.posts.all()
        list_of_all_likes = [p.likes.count() for p in all_user_posts]
        return {**data,
                'post_count': user.posts.count(),
                'fame_index': sum(list_of_all_likes)
                }


class PrivateUserSerializer(UserSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'email', 'post_count', 'fame_index']
        read_only_fields = fields


# class FollowerSerializers(serializers.ModelSerializer):
#     username = serializers.CharField(
#         label='username'
#     )
#     number_of_followers = serializers.CharField(
#         label='number of followers'
#     )
#
#     def to_representation(self, usr_profile_obj):
#         return {'username': usr_profile_obj.user.username,
#                 'number_of_followers': usr_profile_obj.followees.count(),
#                 }
#
#     class Meta:
#         model = UserProfile
#         fields = ['username', 'number_of_followers']


class FollowingSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserProfile
        fields = ['followees']
