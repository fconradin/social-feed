from django.contrib.auth import get_user_model
from django.core.mail import EmailMessage
from rest_framework import serializers
from project.feed.models import UserProfile, random_word

User = get_user_model()


class RegistrationMaster(serializers.Serializer):
    def validate_code(self, value):
        try:
            return User.objects.get(
                user_profile__registration_code=value,
            )
        except User.DoesNotExist:
            raise serializers.ValidationError(
                'Wrong validation code!'
            )


class RegistrationSerializer(RegistrationMaster):
    email = serializers.EmailField(
        label='E-Mail address'
    )

    def validate_email(self, value):
        try:
            User.objects.get(email=value)
            raise serializers.ValidationError(
                'User with this email address is already existing'
            )
        except User.DoesNotExist:
            return value

    def send_registration_email(self, email, code):
        message = EmailMessage(
            subject='Social feed registration',
            body=f'This is your code --> {code}',
            to=[email],
        )
        message.send()

    def register_user(self, email):
        new_user = User.objects.create_user(
            username=email,
            email=email,
            is_active=False,
        )
        user_profile = UserProfile.objects.create(
            user=new_user,
        )
        self.send_registration_email(
            email=email,
            code=user_profile.registration_code,
        )
        return new_user


class RegistrationValidation(RegistrationMaster):
    password = serializers.CharField(
        label='password',
        write_only=True,
    )

    code = serializers.CharField(
        label='Registration Code',
        write_only=True,
    )

    first_name = serializers.CharField(
        label='First Name',
        required=False
    )
    last_name = serializers.CharField(
        label='Last Name',
        required=False
    )

    def save(self, validated_data):
        user = validated_data.get('code')
        if 'first_name' in validated_data:
            user.first_name = validated_data.get('first_name')
        if 'last_name' in validated_data:
            user.last_name = validated_data.get('last_name')
        user.set_password(validated_data.get('password'))
        user.is_active = True
        user.save()
        return user


class PasswordResetSerializer(RegistrationSerializer):
    email = serializers.EmailField(
        label='E-Mail address'
    )

    def validate_email(self, value):
        try:
            User.objects.get(email=value)
            return value
        except User.DoesNotExist:
            raise serializers.ValidationError(
                'No user with this email'
            )

    def change_token(self, email):
        try:
            user_profile = UserProfile.objects.get(user__email=email)
        except UserProfile.DoesNotExist:
            user_profile = UserProfile.objects.create(user__email=email)
        finally:
            user_profile.registration_code = random_word(5)
            user_profile.save()
            self.send_registration_email(email, user_profile.registration_code)


class PasswordResetValidationSerializer(RegistrationMaster):
    password = serializers.CharField(
        label="super secret password"
    )
    code = serializers.CharField(
        label="token"
    )

    def update_password(self, validated_data):
        user = validated_data.get('code')
        user.set_password(validated_data.get('password'))
        user.save()
