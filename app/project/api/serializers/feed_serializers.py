from django.contrib.auth import get_user_model
from rest_framework import serializers

from project.api.serializers.user_serializers import UserSerializer
from project.feed.models import Post

User = get_user_model()


class FeedDisplaySerializer(serializers.ModelSerializer):
    user = UserSerializer()  # we don't need this line of code, do we?

    class Meta:
        model = Post
        fields = ['id', 'content', 'created', 'shared', 'user']
        read_only_fields = ['id', 'created', 'user', 'shared']
