from django.urls import path
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView, TokenVerifyView

import project.api.views.feed_views as feed
import project.api.views.friend_views as friend
import project.api.views.me_views as me
import project.api.views.registration_views as reg
import project.api.views.users_view as user
import project.api.views.post_views as post

app_name = 'api'

urlpatterns = [
    path('feed/', feed.FeedDisplayView.as_view(), name='feed_display'),
    path('feed/<int:user_id>/', feed.FeedDisplayViewUser.as_view(), name='feed_display_user'),
    path('feed/followees/', feed.FeedDisplayViewFollowee.as_view(), name='posts_of_followees'),
    path('feed/friends/', feed.FeedDisplayFriends.as_view(), name='posts_of_friends'),

    path('posts/<int:post_id>/', post.GetUpdateDeleteView.as_view(), name='post_detail'),
    path('posts/<int:post_id>/like/', post.LikeView.as_view(), name='like_post'),
    path('posts/<int:post_id>/unlike/', post.UnLike.as_view(), name='unlike_post'),
    path('posts/new-post/', post.CreateView.as_view(), name='new_post'),
    path('posts/likes/', post.Likes.as_view(), name='post_likes'),
    path('posts/share-post/<int:post_id>/', post.Share.as_view(), name='share_post'),

    path('users/', user.UsersView.as_view(), name='users'),
    path('users/<int:user_id>/', user.SpecificUserView.as_view(), name='specific_user'),
    path('user/follow/<int:user_id>/', user.Follow.as_view(), name='follow'),
    path('user/followers/', user.FollowersView.as_view(), name='followers'),
    path('user/following/', user.FollowingView.as_view(), name='following'),

    path('user/friends/', friend.Friends.as_view(),
         name='friends_view'),
    path('user/friendrequests/', friend.Request.as_view(),
         name='send_friend_request'),
    path('user/friendrequests/<int:user_id>/', friend.SendRequest.as_view(),
         name='send_friend_request'),
    path('user/friendrequests/accept/<int:request_id>/', friend.AcceptRequest.as_view(),
         name='accept_friend_request'),
    path('user/friendrequests/reject/<int:request_id>/', friend.RejectRequest.as_view(),
         name='reject_friend_request'),
    path('user/friends/unfriend/<int:user_id>/', friend.Unfriend.as_view(),
         name='unfriend'),

    path('me/', me.MeView.as_view(), name='me_view'),

    path('auth/registration/', reg.RegistrationView.as_view(), name='registration'),
    path('auth/registration/validation/', reg.RegistrationValidationView.as_view(), name='registration_validation'),
    path('auth/password-reset/', reg.ResetPasswordView.as_view(), name='reset_password'),
    path('auth/password-reset/validation/', reg.ResetPasswordValidate.as_view(), name='reset_password_validate'),
    path('auth/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('auth/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('auth/token/verify/', TokenVerifyView.as_view(), name='token_verify'),

]
