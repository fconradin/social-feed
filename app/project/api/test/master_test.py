import string
from random import choice

from django.contrib.auth import get_user_model
from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework_simplejwt.tokens import RefreshToken


User = get_user_model()


def random_word(length):
    letters = string.ascii_lowercase
    return ''.join(choice(letters) for i in range(length))


class TestWrapper:
    class NoAuthMaster(APITestCase):
        endpoint = None
        url = None
        methods = []
        usrs = {}

        def setUp(self):
            for i in range(1, 4):
                name = f'user_{i}'
                pwd = random_word(5)
                user = User.objects.create_user(
                    username=name,
                    password=pwd,
                )
                refresh = RefreshToken.for_user(user)
                access = refresh.access_token
                self.usrs[name] = {
                    'obj': user,
                    'pwd': pwd,
                    'access': access,
                    'refresh': refresh,
                }

    class AuthMaster(NoAuthMaster):
        endpoint = None
        url = None
        methods = []
        all_methods = ['GET', 'POST', 'DELETE']

        def test_unauthorized_requests(self):
            if self.url is not None:
                for m in self.methods:
                    if m == 'GET':
                        r = self.client.get(self.url)
                    if m == 'POST':
                        r = self.client.post(self.url)
                    self.assertEquals(r.status_code, status.HTTP_401_UNAUTHORIZED)

        def test_method_not_allowed(self):
            self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {self.usrs['user_1']['access']}")
            global r
            if self.url is not None:
                for m in self.all_methods:
                    if m not in self.methods:
                        exec(f'global r; r=self.client.{m.lower()}(self.url)')
                        self.assertEqual(r.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
