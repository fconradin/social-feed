from random import randint

from django.contrib.auth import get_user_model
from django.db.models import Q
from django.urls import reverse
from rest_framework import status

from project.api.test.master_test import TestWrapper, random_word
from project.feed.models import Post, UserProfile, FriendRequest

User = get_user_model()


class FeedTestMaster(TestWrapper.AuthMaster):
    endpoint = None
    url = None
    methods = []
    content = random_word(8)

    def setUp(self):
        super().setUp()
        for item in ['user_1', 'user_2', 'user_3']:
            for i in range(10):
                Post.objects.create(
                    user=self.usrs[item]['obj'],
                    content=f'{self.content}_{i}',
                )


# /api/feed/ GET: lists all the posts of all users in chronological order
# /api/feed/?search=<str:search_string> GET: Search posts of all users and list result in chronological order
class FeedTest(FeedTestMaster):
    endpoint = 'api:feed_display'
    methods = ['GET', 'POST']

    def setUp(self):
        super().setUp()
        self.url = reverse(self.endpoint)

    def test_post_feed_search(self):
        start = randint(0, len(self.content))
        end = randint(0, len(self.content))
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {self.usrs['user_1']['access']}")
        resp = self.client.get(self.url, {'search': self.content[start:end]}, format='json')
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertEqual(resp.data[0].get('content')[0:-2], self.content)

    def test_post_feed(self):
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {self.usrs['user_1']['access']}")
        resp = self.client.get(reverse(self.endpoint), format='json')
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertEqual(len(resp.data), len(Post.objects.all()))
        self.assertEqual(resp.data[0].get('content')[0:-2], self.content)


# /api/feed/<int:user_id>/ GET: lists all the posts of a specific user in chronological order
class TestFeedUser(FeedTestMaster):
    endpoint = 'api:feed_display_user'
    methods = ['GET']

    def setUp(self):
        super().setUp()
        self.url = reverse(self.endpoint, kwargs={"user_id": self.usrs['user_2']['obj'].id})

    def test_post_feed_user_id(self):
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {self.usrs['user_1']['access']}")
        r = self.client.get(reverse(self.endpoint, kwargs={"user_id": self.usrs['user_2']['obj'].id}))
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertEqual(len(r.data), len(Post.objects.filter(user__id=self.usrs['user_2']['obj'].id)))

        r = self.client.get(reverse(self.endpoint, kwargs={"user_id": 99}))
        self.assertEqual(r.status_code, status.HTTP_404_NOT_FOUND)


# /api/feed/followees/ GET: lists all the posts of followed users in chronological order
class TestFeedFollowees(FeedTestMaster):
    endpoint = 'api:posts_of_followees'
    methods = ['GET']

    def setUp(self):
        super().setUp()
        self.url = reverse(self.endpoint)
        up = UserProfile.objects.create(
            user=self.usrs['user_1']['obj'],
        )
        up.followees.add(self.usrs['user_2']['obj'])
        up.followees.add(self.usrs['user_3']['obj'])
        UserProfile.objects.create(
            user=self.usrs['user_2']['obj'],
        )

    def test_feed_followees(self):
        usr = 'user_1'
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {self.usrs[usr]['access']}")
        r = self.client.get(self.url)
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        followees = UserProfile.objects.get(user__username=usr).followees.all()
        self.assertEqual(len(r.data), len(Post.objects.filter(user__in=followees)))

    def test_feed_no_followees(self):
        usr = 'user_2'
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {self.usrs[usr]['access']}")
        r = self.client.get(self.url)
        followees = UserProfile.objects.get(user__username=usr).followees.all()
        self.assertEqual(len(r.data), len(Post.objects.filter(user__in=followees)))


class TestFeedFriends(FeedTestMaster):
    endpoint = 'api:posts_of_friends'
    methods = ['GET']

    def setUp(self):
        super().setUp()
        self.url = reverse(self.endpoint)
        FriendRequest.objects.create(sender_id=self.usrs['user_1']['obj'].id,
                                     receiver_id=self.usrs['user_2']['obj'].id,
                                     status="ACCEPTED")

    def test_feed_friends(self):
        usr = ['user_1', 'user_2']
        for u in usr:
            self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {self.usrs[u]['access']}")
            r = self.client.get(self.url)
            self.assertEqual(r.status_code, status.HTTP_200_OK)
            receivers = FriendRequest.objects.filter(sender__id=self.usrs[u]['obj'].id,
                                                     status="ACCEPTED").values('receiver')
            senders = FriendRequest.objects.filter(receiver__id=self.usrs[u]['obj'].id,
                                                   status="ACCEPTED").values('sender')
            posts = Post.objects.filter(Q(user_id__in=receivers) | Q(user_id__in=senders))
            self.assertEqual(len(r.data), len(posts))
