'''
endpoints to test
/api/users/followers/ GET: List of all the followers
/api/users/following/ GET: List of all the people the user is following
/api/users/follow/<int:user_id>/ POST: follow a user
/api/users/follow/<int:user_id>/ DELETE: unfollow a user
OK /api/users/ GET: Get all the users
OK /api/users/?search=<str:search_string> GET: Search users
OK /api/users/<int:user_id>/ GET: Get specific userprofile

path('users/', user.UsersView.as_view(), name='users'),
path('users/<int:user_id>/', user.SpecificUserView.as_view(), name='specific_user'),
path('user/followers/', user.FollowersView.as_view(), name='followers'),
path('user/following/', user.FollowingView.as_view(), name='following'),
path('user/follow/<int:user_id>/', user.Follow.as_view(), name='follow'),
path('user/unfollow/<int:user_id>/', user.Unfollow.as_view(), name='unfollow'),

'''
from django.urls import reverse
from rest_framework import status

from project.api.test.master_test import TestWrapper
from project.feed.models import User, UserProfile


class UsersTest(TestWrapper.AuthMaster):
    endpoint = 'api:users'
    methods = ['GET']

    def setUp(self):
        super().setUp()
        self.url = reverse(self.endpoint)

    def test_users(self):
        u = 'user_1'
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {self.usrs[u]['access']}")
        r = self.client.get(self.url)
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertEqual(len(r.data), len(User.objects.all()))


class UsersTestSearch(TestWrapper.AuthMaster):
    endpoint = 'api:users'
    methods = ['GET']
    search_string = 's'

    def setUp(self):
        super().setUp()
        self.url = f'{reverse(self.endpoint)}?search={self.search_string}'

    def test_users(self):
        u = 'user_1'
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {self.usrs[u]['access']}")
        r = self.client.get(self.url)
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertEqual(len(r.data),
                         len(User.objects.filter(username__icontains=self.search_string))
                         )


# /api/users/<int:user_id>/ GET: Get specific userprofile
class UserTestProfile(TestWrapper.AuthMaster):
    endpoint = 'api:specific_user'
    methods = ['GET']
    u = 'user_1'

    def setUp(self):
        super().setUp()
        self.url = reverse(self.endpoint, kwargs={'user_id': self.usrs['user_1']['obj'].id})

    def test_specific_profile_logged_in_user(self):
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {self.usrs[self.u]['access']}")
        r = self.client.get(self.url)
        self.assertEqual(r.status_code, status.HTTP_200_OK)

    def test_specific_profile_not_logged_in_user(self):
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {self.usrs[self.u]['access']}")
        self.url = reverse(self.endpoint, kwargs={'user_id': self.usrs['user_2']['obj'].id})
        r = self.client.get(self.url)
        self.assertEqual(r.status_code, status.HTTP_200_OK)

    def test_not_existing_profile(self):
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {self.usrs[self.u]['access']}")
        self.url = reverse(self.endpoint, kwargs={'user_id': 99})
        r = self.client.get(self.url)
        self.assertEqual(r.status_code, status.HTTP_404_NOT_FOUND)


# /api/users/follow/<int:user_id>/ POST: follow a user
# /api/users/follow/<int:user_id>/ DELETE: unfollow a user
class FollowTest(TestWrapper.AuthMaster):
    endpoint = 'api:follow'
    methods = ['POST', 'DELETE']

    def setUp(self):
        super().setUp()
        self.url = reverse(self.endpoint, kwargs={'user_id': self.usrs['user_2']['obj'].id})

    def test_follow(self):
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {self.usrs['user_1']['access']}")
        r = self.client.post(self.url)
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertEqual(len(UserProfile.objects.get(user=self.usrs['user_1']['obj']).followees.all()),
                         1)
        self.assertEqual(r.data, f"You are now following user {self.usrs['user_2']['obj'].id}")

        self.url = reverse(self.endpoint, kwargs={'user_id': self.usrs['user_3']['obj'].id})
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        r = self.client.post(self.url)
        self.assertEqual(r.data, f"You are now following user {self.usrs['user_3']['obj'].id}")
        self.assertEqual(len(UserProfile.objects.get(user=self.usrs['user_1']['obj']).followees.all()),
                         2)

    def test_unfollow(self):
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {self.usrs['user_1']['access']}")
        r = self.client.delete(self.url)
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertEqual(r.data, f"You were never following user {self.usrs['user_2']['obj'].id}")

        user_profile = UserProfile.objects.get(user=self.usrs['user_1']['obj'])
        user_profile.followees.add(self.usrs['user_2']['obj'])
        r = self.client.delete(self.url)
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertEqual(r.data, f"You are no longer following user {self.usrs['user_2']['obj'].id}")


# /api/users/followers/ GET: List of all the followers
class FollowersTest(TestWrapper.AuthMaster):
    endpoint = 'api:followers'
    methods = ['GET']

    def setUp(self):
        super().setUp()
        self.url = reverse(self.endpoint)

    def test_followers_no_userprofile(self):
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {self.usrs['user_1']['access']}")
        r = self.client.get(self.url)
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertEqual(len(r.data), 0)

    def test_followers_user_profile_created(self):
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {self.usrs['user_1']['access']}")
        user_profile = UserProfile.objects.create(user=self.usrs['user_2']['obj'])
        user_profile.followees.add(self.usrs['user_1']['obj'])
        r = self.client.get(self.url)
        self.assertEqual(len(r.data), len(self.usrs['user_1']['obj'].followers.all()))
        self.assertEqual(r.status_code, status.HTTP_200_OK)


# /api/users/following/ GET: List of all the people the user is following
class FollowingTestNoUserProfile(TestWrapper.AuthMaster):
    endpoint = 'api:following'
    methods = ['GET']

    def setUp(self):
        super().setUp()
        self.url = reverse(self.endpoint)

    def test_following_no_user_profile_created(self):
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {self.usrs['user_1']['access']}")
        r = self.client.get(self.url)
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertEqual(len(r.data), 0)

    def test_following_user_profile_created(self):
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {self.usrs['user_1']['access']}")
        user_profile = UserProfile.objects.create(user=self.usrs['user_1']['obj'])
        user_profile.followees.add(self.usrs['user_2']['obj'])
        user_profile.followees.add(self.usrs['user_3']['obj'])
        r = self.client.get(self.url)
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertEqual(len(r.data), len(user_profile.followees.all()))
