from django.contrib.auth import get_user_model
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from project.api.test.master_test import TestWrapper
from project.feed.models import UserProfile

User = get_user_model()


# testing endpoints: /api/auth/token/
class AuthTestTokenObtain(TestWrapper.NoAuthMaster):
    endpoint = 'api:token_obtain_pair'
    url = reverse(endpoint)

    def test_post_obtain_token(self):
        r = self.client.post(self.url, {"username": "user_1", "password": self.usrs['user_1']['pwd']})
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertEqual(len(r.data), 2)


# testing endpoints: /api/token/verify/
class AuthTestVerify(TestWrapper.NoAuthMaster):
    endpoint = 'api:token_verify'
    url = reverse(endpoint)

    def test_verify_token(self):
        r = self.client.post(self.url, {"token": "some stupid token"})
        self.assertEqual(r.status_code, status.HTTP_401_UNAUTHORIZED)
        r = self.client.post(self.url, {"token": f"{self.usrs['user_1']['access']}"})
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertEqual(len(r.data), 0)


# testing endpoints: /api/auth/token/refresh/
class AuthTestRefresh(TestWrapper.NoAuthMaster):
    endpoint = 'api:token_refresh'
    url = reverse(endpoint)

    def test_refresh_token(self):
        r = self.client.post(self.url, {"refresh": "some stupid token"})
        self.assertEqual(r.status_code, status.HTTP_401_UNAUTHORIZED)
        r = self.client.post(self.url, {"refresh": f"{self.usrs['user_1']['refresh']}"})
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertEqual(len(r.data), 1)


# testing endpoints: auth/registration/
class AuthTestRegistration(APITestCase):
    endpoint = 'api:registration'
    url = reverse(endpoint)

    def test_registration(self):
        r = self.client.post(self.url, {"email": "bla bla bla"})
        self.assertEqual(r.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(r.data['email'][0], "Enter a valid email address.")

        r = self.client.post(self.url, {"email": "email@example.com"})
        self.assertEqual(r.status_code, status.HTTP_200_OK)

        r = self.client.post(self.url, {"email": "email@example.com"})
        self.assertEqual(r.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(r.data['email'][0], "User with this email address is already existing")


class AuthValResetMaster(APITestCase):
    reg_url = reverse('api:registration')
    val_url = reverse('api:registration_validation')
    reset_url = reverse('api:reset_password')
    res_val_url = reverse('api:reset_password_validate')
    email = "email@example.com"
    reg_code = None

    def setUp(self):
        self.client.post(self.reg_url, {"email": "email@example.com"})
        self.reg_code = UserProfile.objects.get(user__email="email@example.com").registration_code


# auth/registration/validation/
class AuthTestRegistrationValidation(AuthValResetMaster):

    def test_registration_validation(self):
        r = self.client.post(self.val_url,
                             {"code": f"{self.reg_code}",
                              "password": "some_password2",
                              "first_name": "Dududd",
                              "last_name": "Dadadd"}
                             )
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertEqual(r.data["first_name"], "Dududd")
        self.assertEqual(r.data["last_name"], "Dadadd")

        r = self.client.post(self.val_url,
                             {"code": "12323454",
                              "password": "some_password2",
                              "first_name": "Dududd",
                              "last_name": "Dadadd"}
                             )
        self.assertEqual(r.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(r.data["code"][0], "Wrong validation code!")


# auth/password-reset/
class AuthTestPasswordReset(AuthValResetMaster):

    def test_reset_password(self):
        r = self.client.post(self.reset_url, {"email": "email@example.com"})
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        new_reg_code = UserProfile.objects.get(user__email=self.email).registration_code
        self.assertNotEqual(self.reg_code, new_reg_code)
        self.assertEqual(r.data, "Email has been sent")

        r = self.client.post(self.reset_url, {"email": "whadda@dudu.ch"})
        self.assertEqual(r.status_code, status.HTTP_400_BAD_REQUEST)


# auth/password-reset/validation/
class AuthTestPasswordResetValidate(AuthValResetMaster):

    def setUp(self):
        super().setUp()
        self.client.post(self.reset_url, {"email": f"{self.email}"})

    def reset_password_validate(self):
        # Test if chaning password with new registration code works
        new_reg_code = UserProfile.objects.get(user__email=self.email).registration_code
        r = self.client.post(self.res_val_url,
                             {"code": f"{new_reg_code}", "password": "pass_word88"}
                             )
        self.assertEqual(r.status_code, status.HTTP_200_OK)

        # Test if bad request with empty password
        r = self.client.post(self.res_val_url,
                             {"code": "12344", "password": ""},
                             )
        self.assertEqual(r.status_code, status.HTTP_400_BAD_REQUEST)

        # Test if bad request with bad code
        r = self.client.post(self.res_val_url,
                             {"code": "12344", "password": "pass"},
                             )
        self.assertEqual(r.status_code, status.HTTP_400_BAD_REQUEST)

        # Test if login possible with new password
        r = self.client.post(reverse('api:token_obtain_pair'),
                             {"username": f"{self.email}", "password": "pass_word88"}
                             )
        self.assertEqual(r.status_code, status.HTTP_200_OK)
