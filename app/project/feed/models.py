from random import choice

from django.conf import settings
from django.contrib.auth import get_user_model
from django.db import models


# here are all the model fields https://docs.djangoproject.com/en/2.0/ref/models/fields/
def random_word(length=5):
    letters = '0123456789'
    return ''.join(choice(letters) for i in range(length))


User = get_user_model()


class Post(models.Model):
    content = models.TextField(
        verbose_name='content',
        null=True,
        blank=True
    )
    user = models.ForeignKey(
        verbose_name='user',
        to=settings.AUTH_USER_MODEL,
        on_delete=models.SET_NULL,
        related_name='posts',
        null=True
    )
    created = models.DateTimeField(
        verbose_name='created',
        auto_now=True,
    )
    shared = models.ForeignKey(
        to='self',
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )

    class Meta:
        ordering = ['-created']
        verbose_name = 'Post'
        verbose_name_plural = 'Posts'


class Like(models.Model):
    user = models.ForeignKey(
        verbose_name='user',
        to=settings.AUTH_USER_MODEL,
        on_delete=models.SET_NULL,
        null=True
    )
    post = models.ForeignKey(
        verbose_name='post',
        to='feed.Post',
        related_name='likes',
        on_delete=models.CASCADE,
    )

    class Meta:
        unique_together = [
            ('user', 'post'),
        ]


class UserProfile(models.Model):
    user = models.OneToOneField(
        verbose_name='user',
        to=settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name='user_profile'
    )

    followees = models.ManyToManyField(
        verbose_name='followees',
        to=settings.AUTH_USER_MODEL,
        related_name='followers',
        blank=True
    )

    registration_code = models.CharField(
        verbose_name='Registration Code',
        max_length=15,
        unique=True,
        default=random_word
    )


class FriendRequest(models.Model):
    FRIEND_REQUEST_CHOICES = (
        ('PENDING', 'PENDING'),
        ('ACCEPTED', 'ACCEPTED'),
        ('REFUSED', 'REFUSED')
    )

    sender = models.ForeignKey(
        verbose_name='sender',
        to=settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name='sent_requests',
    )

    receiver = models.ForeignKey(
        verbose_name='receiver',
        to=settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name='received_requests',
    )

    status = models.CharField(
        verbose_name='status',
        choices=FRIEND_REQUEST_CHOICES,
        max_length=15,
    )

    class Meta:
        unique_together = [
            ('sender', 'receiver'),
        ]
