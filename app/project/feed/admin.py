from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin
from .models import Post, Like, UserProfile, FriendRequest

# Register your models here.

User = get_user_model()

UserAdmin.list_display = ['id', 'username', 'email', 'first_name', 'last_name', 'is_active', 'is_staff']


class FriendRequestAdmin(admin.ModelAdmin):
    list_display = ['id', 'sender', 'receiver', 'status']


class PostAdmin(admin.ModelAdmin):
    list_display = ['id', 'user', 'content', 'created', 'shared']


class UserProfileAdmin(admin.ModelAdmin):
    list_display = ['id', 'user', 'get_followers', 'get_followees', 'registration_code']

    def get_followees(self, up):
        return " - ".join([u.username for u in up.followees.all()])

    def get_followers(self, up):
        return " - ".join([u.user.username for u in up.user.followers.all()])


admin.site.unregister(User)
admin.site.register(User, UserAdmin)
admin.site.register(Post, PostAdmin)
admin.site.register(Like)
admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(FriendRequest, FriendRequestAdmin)
